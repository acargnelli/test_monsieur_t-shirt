import React from 'react';
import {books} from './book_item'; 
import './index.css';


const BookDescritption = (props) =>
{ 
    console.log(props.match);
    return(
    books.map( b => {
        if(b.id === props.match.params.id){
            
            return (
                <div>
                    <h1>{b.title}</h1>
                    <img src={b.image}></img>
                    <ul>
                        <li>{b.date}</li>
                        <li>{b.description}</li>
                        <li>{b.author}</li>
                    </ul>
                </div>
            )
        }
    })
    )   
}

export default BookDescritption;