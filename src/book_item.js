import React from 'react';
import { Router, Route, Link } from "react-router-dom";
import './index.css';

export const books = [{
    id : '0',
    title : "Harry Potter and the philosopher's stone",
    author : "J.K Rowling",
    date: 1997,
    description: "Orphaned, young Harry Potter can finally leave his tyrannical Uncle and Aunt Dursley when a curious messenger reveals to him that he is a wizard. At the age of 11, Harry will finally be able to join the legendary school of witchcraft of Hogwarts, find a family worthy of this name and friends, develop his gifts, and prepare his glorious future.",
    image:"https://upload.wikimedia.org/wikipedia/en/6/6b/Harry_Potter_and_the_Philosopher%27s_Stone_Book_Cover.jpg"
  },
  {
    id : '1',
    title: "Hunger Games",
    author:"Suzanne Collins",
    date:2008,
    description : "The hunger games is a novel that unfolds in Panem, an apocalyptic world. The story is centered on a 16-year-old girl, Katniss Everdeen and her struggle for survival in dystopia. Each year, as a punishment for the failed rebellion by District 13, the 12 Panem Districts are forced to pay tribute to the ruthless Capitol regime. The story begins on the day of reaping at District 12. A day that each district is required to offer two tributes, a boy and a girl aged 12 to 18 years to participate in the games. This was going to be the 74th hunger games.In a twist of fate, Prim Katniss’ 12-year-old sister is selected as one of the tributes. Having lost their father at a young age, she isn’t going to let her mother lose her too as she swore always to protect them. She volunteers to take her sister’s place, and together with the baker’s son, Peeta Mellark they represent district 12. What follows is a series of events that will put Katniss in the spotlight both as a source of hope for the oppressed and as an enemy of the Capitol. Her feelings for Peeta will be exploited for the games, which are aired across all the districts.     She will make new friends, enemies, inspire and give hope to many people in Panem thanks to her performance in the games. What makes the hunger games a more thrilling story is the games win or die rule. Will Katniss have the heart to kill Peeta and vice versa, or will they both survive?",
    image : "https://upload.wikimedia.org/wikipedia/en/thumb/d/dc/The_Hunger_Games.jpg/220px-The_Hunger_Games.jpg"
  }
  ];

  
  class book_item extends React.Component {
    
    render () {
        return (
            <div>
            <h1>Hello Monsieur T-shirt ! </h1>
          <ul>
            {books.map(({ id, title,author,image }) => (
              <li><Link to={`/book/${id}`} key={id}> <img src = {image}></img> {title} {author}</Link></li>
            ))}
          </ul>
          </div>
        )
      }
    }
export default book_item;